package com.yanxml.springcloud.eureka.client.order.demo1_quickstart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;

//@EnableEurekaClient //netflix 推荐的. 不推荐使用. 只能服务eureka
@EnableDiscoveryClient //spring cloud官方提供的. 可以后期切换其他注册中心, 比如nacos. 兼容性好.
@SpringBootApplication
public class EurekaClientOrderApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(EurekaClientOrderApplication.class, args);
    }
}
