package com.yanxml.springcloud.eureka.client.order.demo1_quickstart.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class RestConfig {

//    @Bean
//    public RestTemplate generateRestTemplate(){
//        return new RestTemplate();
//    }


    @LoadBalanced
    @Bean
    public RestTemplate generateRestTemplate(){
        return new RestTemplate();
    }
}
