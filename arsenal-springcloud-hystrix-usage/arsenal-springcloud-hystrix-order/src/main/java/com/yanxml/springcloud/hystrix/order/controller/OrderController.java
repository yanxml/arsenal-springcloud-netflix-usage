package com.yanxml.springcloud.hystrix.order.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class OrderController {

    @HystrixCommand(
            fallbackMethod = "fallbackMethod",
            commandProperties = {
                @HystrixProperty(name = "execution.isolation.strategy", value = "THREAD"),
                @HystrixProperty(
                        name = "execution.isolation.thread.timeoutInMilliseconds",
                        value = "200"),
                @HystrixProperty(name = "circuitBreaker.requestVolumeThreshold", value = "10"),
                @HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "50"),
            },
            threadPoolProperties = {
                @HystrixProperty(name = "coreSize", value = "1"),
                @HystrixProperty(name = "maxQueueSize", value = "10"),
                @HystrixProperty(name = "keepAliveTimeMinutes", value = "1000"),
                @HystrixProperty(name = "queueSizeRejectThreshold", value = "8"),
                @HystrixProperty(name = "metrics.rollingStats.numBuckets", value = "12"),
                @HystrixProperty(name = "metrics.rollingStats.timeInMilliseconds", value = "1500")
            })
    @RequestMapping("/createOrder")
    @ResponseBody
    public String createOrder(String userId) {
        try{
            Thread.sleep(1000);
        } catch (Exception exception){
            exception.printStackTrace();
        }
        log.info("Create Order " + userId);
        return "Create Order " + userId;
    }

    public String fallbackMethod(String userId) {
        return "服务降级, 返回默认的用户信息 userId: " + userId;
    }
}
