package com.yanxml.springcloud.hystrix.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;


@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
@EnableHystrix
public class HystrixClientOrderApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(HystrixClientOrderApplication.class, args);
    }
}
