package com.yanxml.springcloud.hystrix.order.feign_client.down_grade;

import com.yanxml.springcloud.hystrix.order.feign_client.UserFeignClient;
import feign.hystrix.FallbackFactory;

public class UserFeignClientFallBackFactory implements FallbackFactory<UserFeignClient> {
    @Override
    public UserFeignClient create(Throwable throwable) {
        return new UserFeignClient() {
            @Override
            public String getUser(String userId) {
                String message = throwable.getMessage();
                return "根据不同的异常返回不同的处理";
            }
        };

    }
}
