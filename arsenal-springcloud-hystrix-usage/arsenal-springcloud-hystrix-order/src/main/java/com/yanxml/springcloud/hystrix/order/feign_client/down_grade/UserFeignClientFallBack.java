package com.yanxml.springcloud.hystrix.order.feign_client.down_grade;

import com.yanxml.springcloud.hystrix.order.feign_client.UserFeignClient;
import org.springframework.stereotype.Component;

@Component
public class UserFeignClientFallBack implements UserFeignClient {

    @Override
    public String getUser(String userId) {
        return "获取用户默认的失败值" + userId;
    }
}
