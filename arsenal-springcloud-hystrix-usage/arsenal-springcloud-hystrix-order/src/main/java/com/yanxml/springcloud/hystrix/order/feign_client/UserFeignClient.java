package com.yanxml.springcloud.hystrix.order.feign_client;

import com.yanxml.springcloud.hystrix.order.feign_client.down_grade.UserFeignClientFallBackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

// case1: fallback
//@FeignClient(name = "arsenal-feign-user", fallback = UserFeignClientFallBack.class)

// case2: fallback_factory
@FeignClient(name = "arsenal-feign-user", fallbackFactory = UserFeignClientFallBackFactory.class)

public interface UserFeignClient {

    @RequestMapping("/getUserInfo")
    public String getUser(String userId);
}
