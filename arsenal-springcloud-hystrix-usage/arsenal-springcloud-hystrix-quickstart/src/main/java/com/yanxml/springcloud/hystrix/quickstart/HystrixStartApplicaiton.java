package com.yanxml.springcloud.hystrix.quickstart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class HystrixStartApplicaiton {
    public static void main(String[] args) {
        ApplicationContext applicationContext =
                SpringApplication.run(HystrixStartApplicaiton.class, args);
    }
}
