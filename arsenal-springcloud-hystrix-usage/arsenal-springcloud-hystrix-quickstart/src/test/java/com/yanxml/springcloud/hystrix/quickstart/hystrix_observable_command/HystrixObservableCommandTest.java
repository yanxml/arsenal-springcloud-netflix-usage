package com.yanxml.springcloud.hystrix.quickstart.hystrix_observable_command;

import org.junit.Test;
import rx.Observable;
import rx.Subscriber;

import java.util.concurrent.CountDownLatch;

public class HystrixObservableCommandTest {
    // 非阻塞
    @Test
    public void observer_no_blocking_Test() throws InterruptedException {
        long beginTime = System.currentTimeMillis();

        HystrixObservableCommandDemo observer = new HystrixObservableCommandDemo("observer");
        Observable<String> observe = observer.observe();
        CountDownLatch latch = new CountDownLatch(1);
        observe.subscribe(new Subscriber<String>() {
            @Override
            public void onStart() {
                System.out.println("订阅开始");
            }

            @Override
            public void onCompleted() {
                System.err.println("Observable, onCompleted");
                latch.countDown();
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("Observable , onError");

            }

            @Override
            public void onNext(String s) {
                System.out.println("Observable , onNext");
            }
        });
        latch.await();
    }

    @Test
    public void to_observer_no_blocking_Test() throws InterruptedException {
        long beginTime = System.currentTimeMillis();

        HystrixObservableCommandDemo observer = new HystrixObservableCommandDemo("toObservable");
        Observable<String> observe = observer.toObservable();
        CountDownLatch latch = new CountDownLatch(1);
        observe.subscribe(new Subscriber<String>() {
            @Override
            public void onStart() {
                System.out.println("订阅开始");
            }

            @Override
            public void onCompleted() {
                System.err.println("toObservable, onCompleted");
                latch.countDown();
            }

            @Override
            public void onError(Throwable throwable) {
                System.out.println("toObservable , onError");

            }

            @Override
            public void onNext(String s) {
                System.out.println("toObservable , onNext");
            }
        });
        latch.await();
    }
}
