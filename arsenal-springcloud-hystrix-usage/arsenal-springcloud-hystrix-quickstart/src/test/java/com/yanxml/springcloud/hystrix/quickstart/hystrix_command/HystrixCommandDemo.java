package com.yanxml.springcloud.hystrix.quickstart.hystrix_command;

import com.netflix.hystrix.HystrixCommand;
import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandProperties;

public class HystrixCommandDemo extends HystrixCommand<String> {
    private String name;

    public HystrixCommandDemo(String name){
        super(Setter.withGroupKey(
                HystrixCommandGroupKey.Factory.asKey("Arsenal - Command"))
                .andCommandPropertiesDefaults(
                        HystrixCommandProperties.defaultSetter()
                                .withRequestCacheEnabled(true) // 请求开启缓存. 默认开启
                )
        );
        this.name = name;
    }

    // 单次调用业务方法
    @Override
    protected String run() throws Exception {
        String result = "Command Hello world name:"+name;
        System.err.println(result+", currentThread - " +Thread.currentThread().getName());
        return result;
    }

    @Override
    protected String getCacheKey() {
        return String.valueOf(name); // 怎样判断是否是同一个key呢? 我们这里传递一个name, 那我们传递的name相同.
    }

}
