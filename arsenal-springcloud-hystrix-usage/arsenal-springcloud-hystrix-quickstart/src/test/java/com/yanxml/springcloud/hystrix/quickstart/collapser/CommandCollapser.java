package com.yanxml.springcloud.hystrix.quickstart.collapser;

import com.netflix.hystrix.*;
import org.assertj.core.util.Lists;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * 泛型参数 1.批量返回的类型. 2. 请求响应的类型 3. 请求的类型.
 */
public class CommandCollapser extends HystrixCollapser<List<String>, String, Integer> {

    private Integer id;

    public CommandCollapser(Integer id){
        super(
                Setter.withCollapserKey(HystrixCollapserKey.Factory.asKey("CommandCollapser"))
                .andCollapserPropertiesDefaults(
                        // 设置合并请求的延迟时间
                        HystrixCollapserProperties.defaultSetter().withTimerDelayInMilliseconds(1000)
                )
        );
        this.id = id;
    }

    /**
     * 请求参数.
     */
    @Override
    public Integer getRequestArgument() {
        return id;
    }

    /**
     * 批量业务处理.
     */
    @Override
    protected HystrixCommand<List<String>> createCommand(Collection<CollapsedRequest<String, Integer>> collection) {
        return new BatchCommand(collection);
    }

    /**
     * 批量处理请求与结果之间的映射关系.
     */
    @Override
    protected void mapResponseToRequests(List<String> strings, Collection<CollapsedRequest<String, Integer>> collection) {
        int count = 0;
        Iterator<CollapsedRequest<String, Integer>> iterator = collection.iterator();
        while (iterator.hasNext()){
            CollapsedRequest<String, Integer> request = iterator.next();
            String result = strings.get(count++);
            request.setResponse(result);
        }
    }

    class BatchCommand extends HystrixCommand<List<String>> {
        private Collection<CollapsedRequest<String, Integer>> collection ;
        public BatchCommand(Collection<CollapsedRequest<String, Integer>> collection){
            super(Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("BatchCommand")));
            this.collection = collection;
        }

        @Override
        protected List<String> run() throws Exception {
            System.err.println("currentThread: "+ Thread.currentThread().getName());
            List<String> result = Lists.newArrayList();
            Iterator<CollapsedRequest<String, Integer>> iterator = collection.iterator();
            while (iterator.hasNext()){
                CollapsedRequest<String, Integer> request = iterator.next();
                Integer reqParam = request.getArgument();

                // 具体业务处理
                result.add("msb - request:"+ reqParam);
            }
            return result;
        }
    }
}
