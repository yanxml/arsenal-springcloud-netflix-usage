package com.yanxml.springcloud.hystrix.quickstart.fail_fallback;

import com.netflix.hystrix.*;

public class HystrixCommandDemo_FailBack extends HystrixCommand<String> {
    private String name;

    // 线程池相关demo
    public HystrixCommandDemo_FailBack(String name){
        super(Setter.withGroupKey(
                HystrixCommandGroupKey.Factory.asKey("Arsenal - Command"))
                .andCommandPropertiesDefaults(
                        HystrixCommandProperties.defaultSetter()
                                .withExecutionTimeoutInMilliseconds(2*1000) // 单个线程超时时间
                                .withExecutionIsolationStrategy(HystrixCommandProperties.ExecutionIsolationStrategy.SEMAPHORE)
                        .withExecutionIsolationSemaphoreMaxConcurrentRequests(2) // 任务执行信号量最大数量
                        .withFallbackIsolationSemaphoreMaxConcurrentRequests(3) // 失败的最大数量
                )
                .andThreadPoolKey(HystrixThreadPoolKey.Factory.asKey("Arsenal - Command"))
                .andThreadPoolPropertiesDefaults(HystrixThreadPoolProperties.defaultSetter()
                        // 启动5线程 单队列
//                        .withCoreSize(2)
//                        .withMaximumSize(5)
//                        .withAllowMaximumSizeToDivergeFromCoreSize(true)

                        // 2队列 都是线程池为3
                        .withCoreSize(2)
                        .withMaximumSize(3)
                        .withAllowMaximumSizeToDivergeFromCoreSize(true)
                        .withMaxQueueSize(2)
                )
        );
        this.name = name;
    }

    @Override
    protected String getFallback() {
        String result = "Fallback name:"+name;
        System.err.println(result+", currentThread - " +Thread.currentThread().getName());
        return result;
    }

    // 单次调用业务方法
    @Override
    protected String run() throws Exception {
        String result = "Command Hello world name:"+name;
        System.err.println(result+", currentThread - " +Thread.currentThread().getName());
        return result;
    }

    static class SemaphoreThread extends Thread {
        private String name;

        public SemaphoreThread(String name){
            this.name = name;
        }

        @Override
        public void run(){
            HystrixCommandDemo_FailBack commandDemo = new HystrixCommandDemo_FailBack(name);
            System.out.println("HystrixCommandDemo result: "+ commandDemo.execute());
        }
    }

}
