package com.yanxml.springcloud.hystrix.quickstart.thread_pool_semaphere;

import org.junit.Test;

public class HystrixCommandTest {


    /**
     * 信号量隔离
     */
    @Test
    public void semaphore_test() throws InterruptedException {
        HystrixCommandDemo.SemaphoreThread thread1 = new HystrixCommandDemo.SemaphoreThread("t1");
        HystrixCommandDemo.SemaphoreThread thread2 = new HystrixCommandDemo.SemaphoreThread("t2");
        HystrixCommandDemo.SemaphoreThread thread3 = new HystrixCommandDemo.SemaphoreThread("t3");
        HystrixCommandDemo.SemaphoreThread thread4 = new HystrixCommandDemo.SemaphoreThread("t4");
        HystrixCommandDemo.SemaphoreThread thread5 = new HystrixCommandDemo.SemaphoreThread("t5");

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        Thread.sleep(2*1000);
    }
}
