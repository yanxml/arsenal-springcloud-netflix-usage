package com.yanxml.springcloud.hystrix.quickstart.hystrix_observable_command;

import com.netflix.hystrix.HystrixCommandGroupKey;
import com.netflix.hystrix.HystrixCommandKey;
import com.netflix.hystrix.HystrixObservableCommand;
import rx.Observable;
import rx.Subscriber;
import rx.schedulers.Schedulers;

public class HystrixObservableCommandDemo extends HystrixObservableCommand<String> {
    private String name;

    protected HystrixObservableCommandDemo(String name) {
        super(
                Setter.withGroupKey(HystrixCommandGroupKey.Factory.asKey("ObserverCommandGroupKey"))
                        .andCommandKey(HystrixCommandKey.Factory.asKey("ObserverCommandKey")));
        this.name = name;
    }

    @Override
    protected Observable<String> construct() {
        System.err.println("current thread: "+Thread.currentThread().getName());

        return Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                // 业务处理 这里可以进行批量处理. 此处和run不一样, 他可以调用多次onNext进行处理
                subscriber.onNext("action 1, name="+ name);
                subscriber.onNext("action 2, name="+ name);
                subscriber.onNext("action 3, name="+ name);
                // 业务处理结束
                subscriber.onCompleted();
            }
        }).subscribeOn(Schedulers.io());
//        return null;
    }
}
