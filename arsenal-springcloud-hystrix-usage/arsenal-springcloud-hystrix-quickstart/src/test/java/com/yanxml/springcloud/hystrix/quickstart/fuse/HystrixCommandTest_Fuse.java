package com.yanxml.springcloud.hystrix.quickstart.fuse;

import org.junit.Test;

public class HystrixCommandTest_Fuse {

    /** 熔断器测试类. */
    @Test
    public void FuseTest_test1() throws InterruptedException {
        // 成过业务
        HystrixCommandDemo_Fuse c1 = new HystrixCommandDemo_Fuse("c1");
        System.out.println(c1.execute());

        // 失败业务
        HystrixCommandDemo_Fuse c2 = new HystrixCommandDemo_Fuse("error-demo");
        System.out.println(c2.execute());

        // 成过业务
        HystrixCommandDemo_Fuse c3 = new HystrixCommandDemo_Fuse("c3");
        System.out.println(c3.execute());
    }

    /** 熔断器测试类. */
    @Test
    public void FuseTest_test2() throws InterruptedException {
        // 成过业务
        HystrixCommandDemo_Fuse c1 = new HystrixCommandDemo_Fuse("c1");
        System.out.println(c1.execute());

        // 失败业务
        HystrixCommandDemo_Fuse c2 = new HystrixCommandDemo_Fuse("error-demo");
        System.out.println(c2.execute());

        // 睡眠1s. 让前面2次调用和第三次调用分配在不同的时间桶内
        Thread.sleep(1000);

        // 成过业务
        HystrixCommandDemo_Fuse c3 = new HystrixCommandDemo_Fuse("c3");
        System.out.println(c3.execute());

    }

    /** 熔断器测试类. */
    @Test
    public void FuseTest_test3() throws InterruptedException {
        // 成过业务
        HystrixCommandDemo_Fuse c1 = new HystrixCommandDemo_Fuse("c1");
        System.out.println(c1.execute());

        // 失败业务
        HystrixCommandDemo_Fuse c2 = new HystrixCommandDemo_Fuse("error-demo");
        System.out.println(c2.execute());

        // 睡眠1s. 让前面2次调用和第三次调用分配在不同的时间桶内
        Thread.sleep(1000);

        // 成过业务
        HystrixCommandDemo_Fuse c3 = new HystrixCommandDemo_Fuse("c3");
        System.out.println(c3.execute());

        Thread.sleep(5000);

        // 成过业务
        HystrixCommandDemo_Fuse c4 = new HystrixCommandDemo_Fuse("c4");
        System.out.println(c4.execute());

        // 失败业务
        HystrixCommandDemo_Fuse c5 = new HystrixCommandDemo_Fuse("c5");
        System.out.println(c5.execute());
    }

    /** 熔断器测试类. */
    @Test
    public void FuseTest_test4() throws InterruptedException {
        // 成过业务
        HystrixCommandDemo_Fuse c1 = new HystrixCommandDemo_Fuse("c1");
        System.out.println(c1.execute());

        // 失败业务
        HystrixCommandDemo_Fuse c2 = new HystrixCommandDemo_Fuse("error-demo");
        System.out.println(c2.execute());

        // 睡眠1s. 让前面2次调用和第三次调用分配在不同的时间桶内
        Thread.sleep(1000);

        // 成过业务
        HystrixCommandDemo_Fuse c3 = new HystrixCommandDemo_Fuse("c3");
        System.out.println(c3.execute());

        Thread.sleep(5000);

        // 成过业务
        HystrixCommandDemo_Fuse c4 = new HystrixCommandDemo_Fuse("error-demo");
        System.out.println(c4.execute());

        // 失败业务
        HystrixCommandDemo_Fuse c5 = new HystrixCommandDemo_Fuse("c5");
        System.out.println(c5.execute());
    }
}
