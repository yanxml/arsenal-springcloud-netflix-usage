package com.yanxml.springcloud.hystrix.quickstart.fail_fallback;

import com.yanxml.springcloud.hystrix.quickstart.thread_pool_semaphere.HystrixCommandDemo;
import org.junit.Test;

public class HystrixCommandTest_FailBack {


    /**
     * 信号量隔离
     */
    @Test
    public void semaphore_test() throws InterruptedException {
        HystrixCommandDemo_FailBack.SemaphoreThread thread1 = new HystrixCommandDemo_FailBack.SemaphoreThread("t1");
        HystrixCommandDemo_FailBack.SemaphoreThread thread2 = new HystrixCommandDemo_FailBack.SemaphoreThread("t2");
        HystrixCommandDemo_FailBack.SemaphoreThread thread3= new HystrixCommandDemo_FailBack.SemaphoreThread("t3");
        HystrixCommandDemo_FailBack.SemaphoreThread thread4 = new HystrixCommandDemo_FailBack.SemaphoreThread("t4");
        HystrixCommandDemo_FailBack.SemaphoreThread thread5 = new HystrixCommandDemo_FailBack.SemaphoreThread("t5");

        thread1.start();
        thread2.start();
        thread3.start();
        thread4.start();
        thread5.start();

        Thread.sleep(2*1000);
    }
}
