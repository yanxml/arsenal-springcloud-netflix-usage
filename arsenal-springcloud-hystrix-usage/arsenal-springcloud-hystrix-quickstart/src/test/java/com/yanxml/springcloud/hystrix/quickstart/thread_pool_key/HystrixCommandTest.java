package com.yanxml.springcloud.hystrix.quickstart.thread_pool_key;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.junit.Test;
import rx.Observable;
import rx.Subscriber;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class HystrixCommandTest {

    @Test
    public void execute_execute() {
        HystrixCommandDemo commandDemo = new HystrixCommandDemo("execute");
        String result = commandDemo.execute();
        System.out.println("result=" + result);
    }

    @Test
    public void execute_execute_time() throws InterruptedException {
        long beginTime = System.currentTimeMillis();
        HystrixCommandDemo commandDemo = new HystrixCommandDemo("execute");
        // 同步执行command. 注意有run和execute方法.
        // 如果之间调用run方法, 前面的彦祖就不会起效果
        String result = commandDemo.execute();
        Thread.sleep(800);
        long endTime = System.currentTimeMillis();
        System.out.println("result=" + result+", speed = "+ (endTime-beginTime));
    }

    @Test
    public void thread_pool_thread_test() throws ExecutionException, InterruptedException {
        HystrixCommandDemo c1 = new HystrixCommandDemo("c1");
        HystrixCommandDemo c2 = new HystrixCommandDemo("c2");
        HystrixCommandDemo c3 = new HystrixCommandDemo("c3");
        HystrixCommandDemo c4 = new HystrixCommandDemo("c4");
        HystrixCommandDemo c5 = new HystrixCommandDemo("c5");

        Future<String> q1 = c1.queue();
        Future<String> q2 = c2.queue();
        Future<String> q3 = c3.queue();
        Future<String> q4 = c4.queue();
        Future<String> q5 = c5.queue();

        String r1 = q1.get();
        String r2 = q2.get();
        String r3 = q3.get();
        String r4 = q4.get();
        String r5 = q5.get();

        System.out.println(r1+ ","+ r2+","+r3+","+r4+","+r5);
    }
}
