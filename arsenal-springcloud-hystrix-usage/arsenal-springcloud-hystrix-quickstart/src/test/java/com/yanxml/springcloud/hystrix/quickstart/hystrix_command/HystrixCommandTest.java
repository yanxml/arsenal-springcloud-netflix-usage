package com.yanxml.springcloud.hystrix.quickstart.hystrix_command;

import com.netflix.hystrix.strategy.concurrency.HystrixRequestContext;
import org.junit.Test;
import rx.Observable;
import rx.Subscriber;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class HystrixCommandTest {

    @Test
    public void execute_execute() {
        HystrixCommandDemo commandDemo = new HystrixCommandDemo("execute");
        // 同步执行command. 注意有run和execute方法.
        // 如果之间调用run方法, 前面的彦祖就不会起效果
        String result = commandDemo.execute();
        System.out.println("result=" + result);
    }

    @Test
    public void execute_execute_time() throws InterruptedException {
        long beginTime = System.currentTimeMillis();
        HystrixCommandDemo commandDemo = new HystrixCommandDemo("execute");
        // 同步执行command. 注意有run和execute方法.
        // 如果之间调用run方法, 前面的彦祖就不会起效果
        String result = commandDemo.execute();
        Thread.sleep(800);
        long endTime = System.currentTimeMillis();
        System.out.println("result=" + result+", speed = "+ (endTime-beginTime));
    }

    @Test
    public void queue() throws ExecutionException, InterruptedException {
        long beginTime = System.currentTimeMillis();
        HystrixCommandDemo commandDemo = new HystrixCommandDemo("queue");
        Future<String> queue = commandDemo.queue();
        long endTime = System.currentTimeMillis();
        System.out.println("future end, speeding = "+ (endTime-beginTime));
        System.out.println("result = "+ queue.get());
        long endTime2 = System.currentTimeMillis();
        System.out.println("queue end, speeding = "+ (endTime2-beginTime));
    }

    @Test
    public void observer_blocking()  {
        long beginTime = System.currentTimeMillis();
        HystrixCommandDemo commandDemo = new HystrixCommandDemo("observe");
        Observable<String> observe = commandDemo.observe();
        // 阻塞方式调用
        String result = observe.toBlocking().single();
        long endTime = System.currentTimeMillis();
        System.out.println("future end, speeding = "+ (endTime-beginTime));
        System.out.println("result = "+ result);
    }

    @Test
    public void observer_no_blocking() throws InterruptedException {
        long beginTime = System.currentTimeMillis();
        HystrixCommandDemo commandDemo = new HystrixCommandDemo("observe");
        Observable<String> observe = commandDemo.observe();
        Thread.sleep(1000);
        // 阻塞方式调用
        CountDownLatch countDownLatch = new CountDownLatch(1);
        observe.subscribe(new Subscriber<String>() {
            @Override
            public void onStart() {
                System.err.println("observer, onStart");
            }

            @Override
            public void onCompleted() {
                countDownLatch.countDown();
                System.err.println("observer, onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                System.err.println("observer, onError, throwable = " + e);
            }

            @Override
            public void onNext(String s) {
                long endTime = System.currentTimeMillis();
                System.err.println("observer onNext, speeding = "+ (endTime-beginTime));
            }
        });
        countDownLatch.await();
    }

    @Test
    public void to_observer_blocking() throws InterruptedException {
        long beginTime = System.currentTimeMillis();
        HystrixCommandDemo commandDemo = new HystrixCommandDemo("to_observe");
        Observable<String> observe = commandDemo.toObservable();
        // 阻塞方式调用
        String result = observe.toBlocking().single();
        long endTime = System.currentTimeMillis();
        System.out.println("future end, speeding = "+ (endTime-beginTime));
        System.out.println("result = "+ result);
    }

    @Test
    public void to_observer_no_blocking() throws InterruptedException {
        long beginTime = System.currentTimeMillis();
        HystrixCommandDemo commandDemo = new HystrixCommandDemo("to_observe");
        Observable<String> observe = commandDemo.toObservable();
        Thread.sleep(1000);
        // 阻塞方式调用
        CountDownLatch countDownLatch = new CountDownLatch(1);
        observe.subscribe(new Subscriber<String>() {
            @Override
            public void onStart() {
                System.err.println("observer, onStart");
            }
            @Override
            public void onCompleted() {
                countDownLatch.countDown();
                System.err.println("to_observe, onCompleted");
            }

            @Override
            public void onError(Throwable e) {
                System.err.println("to_observe, onError, throwable = " + e);
            }

            @Override
            public void onNext(String s) {
                long endTime = System.currentTimeMillis();
                System.err.println("to_observe onNext, speeding = "+ (endTime-beginTime));
            }
        });
        countDownLatch.await();
    }

    @Test
    public void request_no_cache_test(){
        // 开启上下文
        HystrixRequestContext requestContext = HystrixRequestContext.initializeContext();

        long beginTime = System.currentTimeMillis();

        HystrixCommandDemo c1 = new HystrixCommandDemo("c1");
        HystrixCommandDemo c2 = new HystrixCommandDemo("c2");
        HystrixCommandDemo c3 = new HystrixCommandDemo("c3");
        HystrixCommandDemo c4 = new HystrixCommandDemo("c4");

        // 第一次请求
        String r1 = c1.execute();
        System.out.println("result="+r1+", speed="+(System.currentTimeMillis()-beginTime));

        // 第一次请求
        String r2 = c2.execute();
        System.out.println("result="+r2+", speed="+(System.currentTimeMillis()-beginTime));

        // 第一次请求
        String r3 = c3.execute();
        System.out.println("result="+r3+", speed="+(System.currentTimeMillis()-beginTime));
    }

    @Test
    public void request_has_cache_test(){
        // 开启上下文
        HystrixRequestContext requestContext = HystrixRequestContext.initializeContext();

        long beginTime = System.currentTimeMillis();

        HystrixCommandDemo c1 = new HystrixCommandDemo("c1");
        HystrixCommandDemo c2 = new HystrixCommandDemo("c2");
        HystrixCommandDemo c3 = new HystrixCommandDemo("c1");

        // 第一次请求
        String r1 = c1.execute();
        System.out.println("result="+r1+", speed="+(System.currentTimeMillis()-beginTime));

        // 第一次请求
        String r2 = c2.execute();
        System.out.println("result="+r2+", speed="+(System.currentTimeMillis()-beginTime));

        // 第一次请求
        String r3 = c3.execute();
        System.out.println("result="+r3+", speed="+(System.currentTimeMillis()-beginTime));
    }
}
