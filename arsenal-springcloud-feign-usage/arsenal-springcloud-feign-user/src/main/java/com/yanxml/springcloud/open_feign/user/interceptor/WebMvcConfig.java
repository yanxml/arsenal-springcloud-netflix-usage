package com.yanxml.springcloud.open_feign.user.interceptor;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebMvcConfig extends WebMvcConfigurationSupport {

    /**
     * 添加新增的拦截器.
     */
    @Override
    protected void addInterceptors(InterceptorRegistry Registry) {
        Registry.addInterceptor(new AuthorInterceptor());
    }
}
