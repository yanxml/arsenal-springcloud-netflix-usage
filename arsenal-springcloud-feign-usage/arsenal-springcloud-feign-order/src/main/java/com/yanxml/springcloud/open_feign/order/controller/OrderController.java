package com.yanxml.springcloud.open_feign.order.controller;

import com.yanxml.springcloud.open_feign.order.service.IUserServiceAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    private IUserServiceAPI userServiceAPI;

    @RequestMapping("/createOrderFromFeign")
    @ResponseBody
    public String createOrderFromFeign(String userId){
        String userInfo = userServiceAPI.getUserInfoFromFeign(userId);
        return "createOrderFromFeign 创建订单Success"+userInfo;
    }
}
