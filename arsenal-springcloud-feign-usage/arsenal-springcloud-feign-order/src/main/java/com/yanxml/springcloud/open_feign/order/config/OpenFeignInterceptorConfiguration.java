package com.yanxml.springcloud.open_feign.order.config;

import com.yanxml.springcloud.open_feign.order.interceptor.FeignAuthInterceptor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenFeignInterceptorConfiguration {

    @Value("${feign.tokenId}")
    private String tokenId;

    /**
     * 自定义拦截器.
     */
    @Bean
    public FeignAuthInterceptor feignAuthInterceptor(){
        return new FeignAuthInterceptor(tokenId);
    }
}
