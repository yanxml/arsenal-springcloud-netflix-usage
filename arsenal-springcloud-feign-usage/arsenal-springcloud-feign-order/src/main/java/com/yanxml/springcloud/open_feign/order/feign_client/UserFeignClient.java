package com.yanxml.springcloud.open_feign.order.feign_client;

import com.yanxml.springcloud.open_feign.order.config.OpenFeignConfiguration;
import feign.RequestLine;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(name = "arsenal-feign-user", configuration = OpenFeignConfiguration.class)
public interface UserFeignClient {

    @RequestMapping("/getUserInfo")
    public String getUser(String userId);

    // Feign方式进行配置
//    @RequestLine("GET /getUserInfo")
//    public String getUser(String userId);
}
