package com.yanxml.springcloud.open_feign.order.interceptor;

import feign.RequestInterceptor;
import feign.RequestTemplate;

public class FeignAuthInterceptor implements RequestInterceptor {
    private String tokenId;

    public FeignAuthInterceptor(String tokenId){
        this.tokenId = tokenId;
    }

    @Override
    public void apply(RequestTemplate requestTemplate) {
        requestTemplate.header("Authorization", tokenId);
    }
}
