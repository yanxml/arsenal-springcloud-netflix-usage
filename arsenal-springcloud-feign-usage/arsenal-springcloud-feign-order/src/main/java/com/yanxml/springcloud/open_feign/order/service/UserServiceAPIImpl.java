package com.yanxml.springcloud.open_feign.order.service;

import com.yanxml.springcloud.open_feign.order.feign_client.UserFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@Slf4j
public class UserServiceAPIImpl implements IUserServiceAPI {

    @Autowired
    private UserFeignClient userFeignClient;

    @Override
    public String getUserInfoFromFeign(String userId) {
        return userFeignClient.getUser(userId);
    }
}
