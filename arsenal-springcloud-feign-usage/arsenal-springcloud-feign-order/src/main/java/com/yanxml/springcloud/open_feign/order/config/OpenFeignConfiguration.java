package com.yanxml.springcloud.open_feign.order.config;

import feign.Contract;
import feign.Logger;
import feign.Request;
import feign.codec.Decoder;
import feign.codec.Encoder;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;

public class OpenFeignConfiguration {

    @Bean
    public Logger.Level level() {
        return Logger.Level.FULL;
    }

    /**
     * 注意: 此处是不支持SpringMVC注解的.
     */
    @Bean
    public Contract feignContract() {
        return new Contract.Default();

        // 不支持SpringMvc注解.
//        return new SpingMvcConstract();
    }

    /**
     * 自定义编码器
     */
//    @Bean
//    public Encoder encode() {
//        return new SpringEncoder();
//    }

    /**
     * 自定义解码器
     */
//    @Bean
//    public Decoder decoder() {
//        return new SpringDecoder();
//    }

    /**
     *  自定义拦截器
     */


    /**
     * 自定义超时时间
     */
    @Bean
    public Request.Options options(){
        return new Request.Options(2000, 5000);
    }

}
