package com.yanxml.springcloud.open_feign.order.service;

public interface IUserServiceAPI {

    String getUserInfoFromFeign(String userId);

}
