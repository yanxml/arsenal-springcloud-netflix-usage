package com.yanxml.springcloud.open_feign.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;


@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients
public class FeignClientOrderApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(FeignClientOrderApplication.class, args);
    }
}
