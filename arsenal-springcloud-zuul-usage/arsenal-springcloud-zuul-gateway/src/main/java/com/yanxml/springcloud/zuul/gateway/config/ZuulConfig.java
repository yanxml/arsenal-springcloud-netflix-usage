package com.yanxml.springcloud.zuul.gateway.config;

import com.yanxml.springcloud.zuul.gateway.filter.MyFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ZuulConfig {

    @Bean
    public MyFilter myFilter() {
        return new MyFilter();
    }
}
