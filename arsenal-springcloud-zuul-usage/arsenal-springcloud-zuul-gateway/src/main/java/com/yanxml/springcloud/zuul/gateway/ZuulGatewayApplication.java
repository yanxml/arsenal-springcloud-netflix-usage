package com.yanxml.springcloud.zuul.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.netflix.zuul.EnableZuulServer;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationContext;


@SpringBootApplication
@EnableZuulProxy // EnableZuulProxy提供更多过滤器 EnableZuulServer
public class ZuulGatewayApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(ZuulGatewayApplication.class, args);
    }
}
