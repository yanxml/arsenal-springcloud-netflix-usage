package com.yanxml.springcloud.zuul.gateway.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import lombok.extern.slf4j.Slf4j;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

@Slf4j
public class MyFilter extends ZuulFilter {
    /** filter 类型 */
    @Override
    public String filterType() {
        return "pre";
    }

    /** filter 执行顺序. 越小优先级越高. */
    @Override
    public int filterOrder() {
        return 0;
    }

    /** 是否开启.这里定制化变成提供方便, 比如为不同的公司提供不同的方案. */
    @Override
    public boolean shouldFilter() {
        return false;
    }

    /** Filter具体业务逻辑. */
    @Override
    public Object run() throws ZuulException {
        RequestContext requestContext = RequestContext.getCurrentContext();
        HttpServletRequest request = requestContext.getRequest();
        Enumeration<String> headerNames = request.getHeaderNames();

        while (headerNames.hasMoreElements()) {
            String headName = headerNames.nextElement();
            log.error("headName:{}, headValue:{}", headName, request.getHeader(headName));
        }

        return null;
    }
}
