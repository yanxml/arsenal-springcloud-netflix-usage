package com.yanxml.springcloud.eureka.client.order.ribbon.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.ZoneAvoidanceRule;
import org.springframework.context.annotation.Bean;

public class UserRibbonConfiguration {

    @Bean
    public IRule ribbonRule(){
        return new ZoneAvoidanceRule();
    }
}
