package com.yanxml.springcloud.eureka.client.order.ribbon.server;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceAPIImpl implements IUserServiceAPI {

    @Autowired private RestTemplate restTemplate;

    @Autowired private LoadBalancerClient eurekaClient;

    @Autowired private DiscoveryClient discoveryClient;

    @Override
    public String getUserInfo(String userId) {
        // 原始调用
        String hostname = "localhost";
        int port = 9021;
        String uri = "/getUserInfo?userId=" + userId;
        String url = "http://" + hostname + ":" + port + uri;
        String result = restTemplate.getForObject(url, String.class);
        return result;
    }

    /** 服务端发现demo. */
    @Override
    public String getUserInfoFromEureka(String userId) {
        ServiceInstance instance = eurekaClient.choose("arsenal-ribbon-client-user");
        int port = instance.getPort();
        String hostname = instance.getHost();
        String uri = "/getUserInfo?userId=" + userId;
        String url = "http://" + hostname + ":" + port + uri;
        String result = restTemplate.getForObject(url, String.class);
        return result;
    }

    /** 客户端负载均衡demo. */
    @Override
    public String getUserInfoByLoadBalance(String userId) {
        List<ServiceInstance> instances =
                discoveryClient.getInstances("arsenal-ribbon-client-user");
        // 数据变化 获取所有实例对应的url
        List<String> targetUrlList =
                instances.stream()
                        .map(
                                instance -> {
                                    return instance.getUri().toString()
                                            + "/getUserInfo?userId="
                                            + userId;
                                })
                        .collect(Collectors.toList());
        // 随机获取一个
        int i = ThreadLocalRandom.current().nextInt(targetUrlList.size());
        String targetUrl = targetUrlList.get(i);
        log.info("请求的目标地址:{}", targetUrl);
        String result = restTemplate.getForObject(targetUrl + userId, String.class);
        return result;
    }

    /** Ribbon. */
    @Override
    public String getUserInfoByLoadRibbon(String userId) {
        String result =
                restTemplate.getForObject(
                        "http://arsenal-ribbon-client-user/getUserInfo?userId=" + userId,
                        String.class);
        String stock =
                restTemplate.getForObject(
                        "http://arsenal-ribbon-client-stock/getUserInfo?userId=" + userId,
                        String.class);
        return "result:"+result+ " stock:"+stock;
    }
}
