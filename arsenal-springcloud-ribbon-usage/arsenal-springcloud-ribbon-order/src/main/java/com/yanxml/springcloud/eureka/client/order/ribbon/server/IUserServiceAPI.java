package com.yanxml.springcloud.eureka.client.order.ribbon.server;

public interface IUserServiceAPI {

    String getUserInfo(String userId);

    String getUserInfoFromEureka(String userId);

    String getUserInfoByLoadBalance(String userId);

    String getUserInfoByLoadRibbon(String userId);

}
