package com.yanxml.springcloud.eureka.client.order.ribbon.controller;

import com.yanxml.springcloud.eureka.client.order.ribbon.server.IUserServiceAPI;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {

    @Autowired
    private IUserServiceAPI userServiceAPI;

    @RequestMapping("/createOrder")
    @ResponseBody
    public String createOrder(String userId){
        String userInfo = userServiceAPI.getUserInfo(userId);
        return "创建订单Success"+userInfo;
    }

    @RequestMapping("/createOrderFromEureka")
    @ResponseBody
    public String createOrderFromEureka(String userId){
        String userInfo = userServiceAPI.getUserInfoFromEureka(userId);
        return "createOrderFromEureka 创建订单Success"+userInfo;
    }

    @RequestMapping("/createOrderFromLoadBalance")
    @ResponseBody
    public String createOrderFromLoadBalance(String userId){
        String userInfo = userServiceAPI.getUserInfoByLoadBalance(userId);
        return "createOrderFromLoadBalance 创建订单Success"+userInfo;
    }
}
