package com.yanxml.springcloud.eureka.client.order.ribbon.config;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import org.springframework.context.annotation.Bean;

public class StockRibbonConfiguration {

    @Bean
    public IRule ribbonRule(){
        // 随机选择
        return new RandomRule();
    }
}
