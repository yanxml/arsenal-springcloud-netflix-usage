package com.yanxml.springcloud.eureka.client.order.ribbon.config;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.netflix.ribbon.RibbonClients;
import org.springframework.context.annotation.Configuration;

@Configuration

// 混合配置方式
@RibbonClients({
        @RibbonClient(name = "arsenal-ribbon-client-user", configuration = UserRibbonConfiguration.class),
        @RibbonClient(name = "arsenal-ribbon-client-stock", configuration = StockRibbonConfiguration.class)
})

// 全局配置方式
//@RibbonClients(defaultConfiguration=UserRibbonConfiguration.class)

public class RibbonConfiguration {}
