package com.yanxml.springcloud.ribbon.user;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;


@EnableDiscoveryClient
@SpringBootApplication
public class RibbonClientUserApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(RibbonClientUserApplication.class, args);
    }
}
