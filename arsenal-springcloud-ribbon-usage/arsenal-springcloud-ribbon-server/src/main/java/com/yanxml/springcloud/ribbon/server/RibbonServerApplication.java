package com.yanxml.springcloud.ribbon.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
@EnableEurekaServer
public class RibbonServerApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(RibbonServerApplication.class, args);

    }
}
