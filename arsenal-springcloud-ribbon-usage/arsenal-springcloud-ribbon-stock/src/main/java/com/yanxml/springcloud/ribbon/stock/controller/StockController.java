package com.yanxml.springcloud.ribbon.stock.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class StockController {

    @Value("${server.port}")
    private int port;

    @RequestMapping("/getStockInfo")
    @ResponseBody
    public String getStockInfo(String userId){
        log.info("获取用户信息Id:{}, port:{}", userId, port);
        return "yanxml";
    }

}
