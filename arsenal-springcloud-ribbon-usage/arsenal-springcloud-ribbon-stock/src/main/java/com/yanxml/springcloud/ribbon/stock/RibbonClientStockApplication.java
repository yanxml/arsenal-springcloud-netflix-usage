package com.yanxml.springcloud.ribbon.stock;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.ApplicationContext;


@EnableDiscoveryClient
@SpringBootApplication
public class RibbonClientStockApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(RibbonClientStockApplication.class, args);
    }
}
