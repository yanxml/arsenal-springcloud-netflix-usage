package com.yanxml.springcloud.eureka.cluster.server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;
import org.springframework.context.ApplicationContext;

@EnableEurekaServer
@SpringBootApplication
public class EurekaServerStartApplication2 {
    public static void main(String[] args) {
        ApplicationContext applicationContext =
                SpringApplication.run(EurekaServerStartApplication2.class, args);
    }
}
